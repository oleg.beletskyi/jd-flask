FROM centos

ADD . /app
WORKDIR /app

RUN yum install -y python3 python3-pip

RUN pip3 install flask

EXPOSE 5000

ENTRYPOINT ["python3", "/app/jd-flask.py"]
